<?php
    define( "CANTIDAD_DE_SIGNOS", 12 );

    $response = new stdClass();
    $response->horoscopo = array();

    $signos = array("Aries","Tauro","Geminis","Cancer","Leo","Virgo","Libra","Escorpio","Sagitario","Capricornio","Acuario","Picsis");

    $signosDescripcion = array(
        "Horoscopo Aries",
        "Horoscopo Tauro",
        "Horoscopo Geminis",
        "Horoscopo Cancer",
        "Horoscopo Leo",
        "Horoscopo Virgo",
        "Horoscopo Libra",
        "Horoscopo Escorpio",
        "Horoscopo Sagitario",
        "Horoscopo Capricornio",
        "Horoscopo Acuario",
        "Horoscopo Picsis"
    );

    for($i=0; $i<CANTIDAD_DE_SIGNOS; $i++)
    {
        $infoSigno = new stdClass();
        $infoSigno->signo = $signos[$i];
        $infoSigno->descripcion = $signosDescripcion[$i];
        $infoSigno->icono = strtolower($signos[$i]) . ".png";

        //array_push($response->horoscopo, array($signos[$i] => $infoSigno));
        array_push($response->horoscopo, $infoSigno);
    }

    header("Content-Type:application/json");

    echo json_encode($response);
