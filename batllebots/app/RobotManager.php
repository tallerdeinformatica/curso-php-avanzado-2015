<?php
/**
 * RobotManager
 */
class RobotManager
{
    const ROBOT_UNO = 1;
    const ROBOT_DOS = 2;

    private $frsRobot = null;
    private $scnRobot = null;

    private $turn = 1;

    private $robot = null;

    public function __construct( $r1 ) {
        $this->frsRobot = $r1;
        $this->scnRobot = $this->asignarEnemigo();

        $this->turn = mt_rand( self::ROBOT_UNO , self::ROBOT_DOS );
    }

    public function comenzar()
    {
        $robot        = ($this->turn == self::ROBOT_UNO) ? $this->frsRobot : $this->scnRobot;
        $robotEnemigo = ($this->turn == self::ROBOT_UNO) ? $this->scnRobot : $this->frsRobot;

        $stats = new stdClass();

        if ($robot && $robotEnemigo)
        {
            $stats = $robot->turno();

            if( $stats ) {
                $energiaFinal = $robotEnemigo->obtenerEnergia() - $stats->points;
                $robotEnemigo->actualizarRobot( $robotEnemigo->obtenerId(), $energiaFinal);
            }
        }

        $this->cambiarTurno();
    }

    private function asignarEnemigo() {
        $robotId = $this->frsRobot->encontrarRobotPorNivel();
        $robot = new Robot();
        $robot->setMyRobot( $robotId );
        return $robot;
    }

    private function cambiarTurno() {
        $this->turn = ($this->turn == self::ROBOT_UNO) ? self::ROBOT_DOS : self::ROBOT_UNO;
    }
}