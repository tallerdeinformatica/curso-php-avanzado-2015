<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Administración</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="container">
    <h1 style="margin-top:5%;">Administración</h1>
    <div class="row">
<?php
    if(isset($_GET["msg"]))
    {
?>
    <p style="background-color:#F3F3F3; padding:5px; text-align:center;">
<?php
        switch($_GET["msg"]) {
          case "user_deleted":
            echo "¡El usuario fue borrado exitosamente!";
            break;
          case "exito":
            echo "¡El usuario fue creado exitosamente!";
            break;
        }
?>
  </p>
<?php
    }
?>
    </div>
    <div class="row">
      <div style="text-align:center;">
        <a class="button button-primary" href="alta.php">Agregar usuario</a>
        <a class="button" href="busqueda.php">Buscar usuario</a>
      </div>
    </div>
    <div class="row">
      <table class="u-full-width">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Robot</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
<?php
  foreach( $listaUsuarios as $usr ) {
?>
        <tr>
          <td><?php echo $usr->nombre ?></td>
          <td><?php echo $usr->direccion ?></td>
          <td>
            <a class="button" href="edicion.php?id=<?php echo $usr->id ?>">Editar</a>
            <a class="button" href="borrado.php?id=<?php echo $usr->id ?>">Eliminar</a>
          </td>
        </tr>
<?php
  }
?>
        </tbody>
      </table>
    </div>
  </div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>