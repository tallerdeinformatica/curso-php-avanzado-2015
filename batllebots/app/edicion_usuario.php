<?php
include_once('models/Usuario.php');
include_once('models/Robot.php');

if( isset($_POST) && isset($_FILES) )
{
  // Input hidden en el código
  $userid = intval($_POST['idUsuario']);

  $data = new stdClass();
  $data->nombre    = filter_var($_POST['nombre'], FILTER_SANITIZE_SPECIAL_CHARS);
  $data->email     = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
  $data->direccion = filter_var($_POST['direccion'], FILTER_SANITIZE_SPECIAL_CHARS);
  $data->telefono  = filter_var($_POST['telefono'], FILTER_SANITIZE_SPECIAL_CHARS);
  $data->nombreRobot  = filter_var($_POST['nombreRobot'], FILTER_SANITIZE_SPECIAL_CHARS);

  // Instanciamos un robot
  $robot = new Robot();
  $robotData = $robot->obtenerRobotUsuario($userid);

  // Subimos el nuevo archivo
  $imagenRobot = $_FILES['imagenRobot'];
 
  if( $imagenRobot ) {
    $tiposArchivo = array('jpg', 'jpeg', 'gif', 'png');
    
    $tempFile = $imagenRobot['tmp_name'];
    $nombreArchivo = filter_var($imagenRobot['name'], FILTER_SANITIZE_SPECIAL_CHARS);
    $destinoFinal = dirname(__FILE__) . '/../upload/';

    $partesNombres = explode('.', strtolower($nombreArchivo));
    $nuevoNombreImagen = str_replace(' ', '_', $partesNombres[0]);
    $extension = end($partesNombres);
    $nuevoNombreImagen.= '_' . md5(uniqid()) . '.' . $extension;

    $archivoFinal = $destinoFinal . $nuevoNombreImagen;
  }

  if( $data->nombre && $data->email && $data->nombreRobot ) 
  {
    $usuario = new Usuario();
    $pass = $usuario->editarUsuario( $userid, $data );

    if( $pass ) {

      $nuevaData = new stdClass();
      $nuevaData->nombre = $data->nombre;
      $nuevaData->avatar = $nuevoNombreImagen;

      // Eliminamos el archivo anterior
      if( $robotData->avatar && $imagenRobot ) {
        if( file_exists($destinoFinal . $robotData->avatar)) {
          unlink($destinoFinal . $robotData->avatar);  
        }
      }

      if( $robot->editarRobot($robotData->id, $nuevaData) ) 
      {
        if( $imagenRobot ) {
          if( in_array($extension, $tiposArchivo) ) {
            //Hago la subida
            if( copy($tempFile, $archivoFinal) ) {
              header("location:../index.php?msg=edicion_exito");
            }else{
              header("location:../index.php?msg=error_copiando_archivo");
            }
          }else{
            // La extension no está permitida
            header("location:../index.php?msg=edicion_extension_archivo");
          }
        }

      }else{
        header("location:../index.php?msg=error_edicion_robot");
      }
    }else{
      header("location:../index.php?msg=edicion_error");
    }
  }else{
    header("location:../index.php?msg=error_datos_requeridos");
  }

}else{
  header("location:../index.php?msg=no_data");
}