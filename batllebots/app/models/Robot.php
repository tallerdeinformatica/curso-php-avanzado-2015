<?php
include_once('Model.php');

/**
 * Usuario
 */
class Robot extends Model
{
    // Variable privada que almacena el objeto PDO
    private $_db;

    const TIEMPO_PARA_ESPECIAL = 20;

    const AUMENTO_NIVEL_NUM    = 10; // CONSTANTES SIEMPRE EN MAYUSCULA

    const NUMERO_ATAQUES       = 3;

    const GANASTE_PELEA        = 1;
    const DEFENDERSE           = 2; // Esto es nuevo
    const ESPERA_PROX_TURNO    = 3;

    const TURNO_ROBOT_PROPIO   = 1;
    const TURNO_ROBOT_ENEMIGO  = 2;

    private $id; // Faltó declarar esta propiedad

    private $experience = 0;
    private $ataques = null;
    private $ataqueActual = null;
    private $tiempoEspecial = 0;
    private $idRobotEnemigo = 0;
    private $robotEnemigo = null;
    private $exp_per_level = 0;
    private $level = 1; // Esto debe de comenzar en 1
    private $energy = 0;
    private $attack = 0;
    private $defense = 0;
    private $estadoActual = 1;

    private $robotActual = null;

    private $stats = null;

    /**
     * [__construct description]
     */
    public function __construct() {
        // Creamos una nueva conexión
        $this->_db = self::getInstance();

        $ataque1 = new stdClass();
        $ataque1->nombre = "Ataque Simple";
        $ataque1->max = 7;
        $ataque1->min = 4;

        $ataque2 = new stdClass();
        $ataque2->nombre = "Ataque Medio";
        $ataque2->max = 11;
        $ataque2->min = 6;

        $ataque3 = new stdClass();
        $ataque3->nombre = "Ataque Fuerte";
        $ataque3->max = 14;
        $ataque3->min = 8;

        $this->ataques = array( $ataque1, $ataque2, $ataque3 );

        $this->stats = new stdClass();
    }

    public function listarRobots() {
      $sql = null;

      try {
            $sql = $this->_db->query("SELECT * FROM robots");
            $sql->setFetchMode(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $sql;
    }

    public function filtrarRobots( $busqueda, $tipo_filtro = self::NOMBRE_EXACTO ) {
      $result = null;

      try {
            $filtrado = "='".$busqueda."'";

            switch( $tipo_filtro ) {
              case self::NOMBRE_EXACTO:
                $filtrado = $busqueda;
                break;
              case self::EMPIEZA_CON:
                $filtrado = $busqueda ."%";
                break;
              case self::TERMINA_CON:
                $filtrado = "%" . $busqueda;
                break;
            }

            $sql = $this->_db->prepare("SELECT * FROM robots WHERE nombre LIKE :Filtrado");
            $sql->execute(array(
              "Filtrado" => $filtrado
            ));
            $sql->setFetchMode(PDO::FETCH_OBJ);
            $result = $sql->fetchAll();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    public function obtenerId()
    {
      return $this->id;
    }

    public function obtenerEnergia() {
      return $this->energy;
    }

    public function obtenerRobot( $id_robot ) {
      $result = null;

      try {
          $sql = $this->_db->prepare("SELECT * FROM robots WHERE id=:IdRobot");
          $sql->execute(array( "IdRobot" => $id_robot ));
          $sql->setFetchMode(PDO::FETCH_OBJ);
          $result = $sql->fetch();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    public function obtenerRobotUsuario( $id_usuario ) {
      $result = null;

      try {
          $sql = $this->_db->prepare("SELECT * FROM robots WHERE id_usuario=:IdUsuario");
          $sql->execute(array( "IdUsuario" => $id_usuario ));
          $sql->setFetchMode(PDO::FETCH_OBJ);
          $result = $sql->fetch();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    /**
     * Agregamos un robot
     * @param  stdClass $data
     * @return Boolean
     */
    public function agregarRobot( $userid, stdClass $data ) {
        $sql = null;

        try {
            $sql= $this->_db->prepare ("INSERT INTO robots (id_user, name, avatar) VALUES(:Id, :Nombre, :Avatar)");
            $sql->execute(array(
              "Id"    => (int) $userid,
              "Nombre" => $data->nombre,
              "Avatar" => $data->avatar
            ));
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return ($sql->rowCount() > 0) ? true : false;
    }

    public function editarRobot( $robotid, stdClass $data ) {
        $sql = null;

        try {
            $sql= $this->_db->prepare ("UPDATE robots SET name=:Nombre, avatar=:Avatar WHERE id=:IdRobot");
            $sql->execute(array(
              "Nombre"    => $data->nombre,
              "Avatar"    => $data->avatar,
              "IdRobot"   => $robotid
            ));
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return ($sql->rowCount() > 0) ? true : false;
    }


    public function setMyRobot( $idRobot )
    {
      $this->id = $idRobot;

      $robot = $this->obtenerRobot($idRobot);

      $this->experience    = (int) $robot->exp;
      $this->exp_per_level = (int) $robot->exp_per_level;
      $this->level         = (int) $robot->level;
      $this->energy        = (int) $robot->energy;
      $this->attack        = (int) $robot->attack;
      $this->defense       = (int) $robot->defense;

      return $robot;
    }

//metodos
    public function aumentarExperiencia($idRobot, $earned_exp){
      $robot= null;

      if(!$this->experience && !$this->exp_per_level) {
        $robot = $this->setMyRobot( $idRobot );
      }

      $expTotal= $this->experience + $earned_exp;
      if($expTotal >= $this->exp_per_level) {
        $this->aumentarNivel($idRobot, $expTotal);
      }else{
        $this->experience = $expTotal;
      }
    }

    public function actualizarRobot( $idRobot, $energia )
    {
        $sql = null;

        try {
          $sql = $this->_db->prepare("UPDATE robots SET energy = :Energy WHERE id=:idRobot");
          $sql->execute(array(
              "Energy"    => $energia,
              "idRobot"   => $idRobot
            ));

        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $sql;
    }

    private function aumentarNivel($idRobot, $expTotal) {
        $this->level++;
        $this->experience = $expTotal - $this->exp_per_level;
        $this->exp_per_level += $this->level * 10 + self::AUMENTO_NIVEL_NUM;
        $this->energy += mt_rand(7,10);
        $this->attack += mt_rand(3,6);
        $this->defense += mt_rand(2,5);

        $sql = null;

        try {
          $sql = $this->_db->prepare("UPDATE robots SET level=:Level, exp_per_level=:ExpPorNivel, experience=:Experience, attack=:Attack, energy=:Energy, defense=:Defense WHERE id=:idRobot");
          $sql->execute(array(
              "Level"        => $this->level,
              "ExpPorNivel"  => $this->exp_per_level,
              "Experience"   => $this->experience,
              "Attack"    => $this->attack,
              "Energy"    => $this->energy,
              "Defense"   => $this->defense,
              "idRobot"   => $this->idRobot
            ));

        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }
    }

    public function turno()
    {
      $atacaDefiende = mt_rand(1,2);
      return ($atacaDefiende == 1) ? $this->atacar() : $this->defender();
    }

    public function encontrarRobotPorNivel() {
      $result = null;

      try {
        $sql = $this->_db->prepare("SELECT id FROM robots WHERE id != :IdRobot AND level = :Nivel");
        $sql->execute(array(
            "IdRobot" => $this->id,
            "Nivel"   => $this->level
          ));

        $sql->setFetchMode(PDO::FETCH_OBJ);
        $result = $sql->fetch();

      } catch (Exception $e) {
        $this->_error = $e->getMessage();
      }

      return ($result) ? $result->id : $result;
    }

    /*
     * ATACAR
     */
    private function atacar()
    {
      if ($this->tiempoEspecial == self::TIEMPO_PARA_ESPECIAL) {
        $this->ataqueActual = $this->ataques[self::NUMERO_ATAQUES - 1];
      } else {
        $this->ataqueActual = $this->ataques[mt_rand(0,self::NUMERO_ATAQUES - 2)]; // Penultimo elemento del array
      }

      $danioGen = mt_rand($this->ataqueActual->min, $this->ataqueActual->max);
      $totalEnergiaPerdida = ($danioGen + $this->defense * .5);

      $this->stats->status = true;
      $this->stats->msg = 'OK';
      $this->stats->type = 'attack';
      $this->stats->points = $totalEnergiaPerdida;

      return $this->stats;
    }

    private function defender()
    {
      if ($this->tiempoEspecial == self::TIEMPO_PARA_ESPECIAL) {
        $this->ataqueActual = $this->ataques[self::NUMERO_ATAQUES - 1];
      } else {
        $this->ataqueActual = $this->ataques[mt_rand(0,self::NUMERO_ATAQUES - 2)]; // Penultimo elemento del array
      }

      $defensa = mt_rand($this->defense-2, $this->defense);

      $this->stats->status = true;
      $this->stats->msg = 'OK';
      $this->stats->type = 'defense';
      $this->stats->points = $defensa;

      return $this->stats;
    }
  // End Robot Class
}