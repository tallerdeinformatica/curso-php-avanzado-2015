<!-- Header & Logo -->
<div class="row">
    <div class="one columns">
      <h1 id="logo"><a href="#"><img src="images/batllebots_logo.png" alt="BatlleBots!" /></a></h1>
    </div>
    <div class="eleven columns">
      <div id="frmLogin" class="u-pull-right" style="margin-top:20px;">
        <?php if( !isset($_SESSION['userLogged']) ) { ?>
        <form action="#" method="POST">
          <label for="txtUser">Usuario</label>
          <input type="text" id="txtUser" name="txtUser" />
          <label for="txtPass">Password</label>
          <input type="password" id="txtPass" name="txtPass" />
          <br />
          <input type="submit" value="Login"/>
        </form>
        <?php } ?>
      </div>
    </div>
</div>
<!-- Navegación -->
<div class="row">
  <nav class="mainNavigation">
      <ul>
          <li><a href="#">Inicio</a></li>
          <li><a href="?section=jugar">Jugar</a></li>
          <li><a href="?section=perfil">Mi perfil</a></li>
          <li><a href="?section=registro" class="specialOption">Crear tu cuenta</a></li>
      </ul>
  </nav>
</div>