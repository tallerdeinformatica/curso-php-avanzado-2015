<?php
include_once("inc/includes.php");

if (isset($_SESSION["userLogged"]))
  $existeUsuario = $_SESSION["userLogged"];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>BatlleBots!</title>
  <meta name="description" content="BatlleBots!">
  <meta name="author" content="">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/general.css">
  <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
    <div class="mainBackground"></div>
    <div class="container">
        <!-- INCLUSIÓN DE HEADER -->
        <?php include_once('inc/header.php'); ?>
        <!-- FIN INCLUSIÓN DE HEADER -->
        <div id="mainContent">
          <div id="pageContent" class="eight columns">
            <h1>Bienvenidos!</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce semper nisl justo, sed mollis neque convallis sit amet. Suspendisse rutrum arcu vitae lectus accumsan, sit amet consectetur nisl porta. Aenean vestibulum eros lorem. Ut libero metus, accumsan eu maximus at, lobortis eget lorem. Vivamus vitae velit elit. Praesent ornare aliquam blandit. Nulla viverra convallis dui cursus pellentesque. Pellentesque turpis ante, pellentesque ut dui ut, egestas porttitor sem. Maecenas placerat tincidunt aliquet. Proin elementum pharetra odio, eu feugiat magna interdum sit amet.</p>
            <p>Cras eget euismod lorem. Sed efficitur vel sapien eu aliquet. Mauris sagittis ligula quis malesuada vehicula. Sed ut tristique nulla, venenatis blandit massa. In et massa tincidunt, vehicula nisl ac, facilisis nulla. Sed sit amet rutrum orci. Vestibulum et tellus non lacus elementum mollis. Quisque in auctor odio. Nunc iaculis ut neque id posuere. Etiam id quam eget lectus eleifend cursus id vel massa. Praesent quis vehicula nibh, sit amet sagittis elit. Pellentesque tincidunt magna at lorem ultrices, consectetur fringilla diam facilisis.</p>
          </div>
          <div id="sideContent" class="four columns">
            <h1>Ranking</h1>
            <aside class="mainMenu">
              <ol style="padding:20px;">
                  <li><a href="#">Federico</a></li>
                  <li><a href="#">Alfonso</a></li>
                  <li><a href="#">Roberto</a></li>
                  <li><a href="#">Edgardo</a></li>
                  <li><a href="#">Anibal</a></li>
              </ol>
            </aside>
          </div>
        </div>

    </div>
    <?php
      include_once("inc/footer.php");
    ?>
</body>
</html>
<?php
  include_once("app/RobotManager.php");
  $debug = true;

  $idRobot = 7;

  $robot = new Robot();
  $robot->setMyRobot( $idRobot );

  $manager = new RobotManager( $robot );
  $manager->comenzar();

  if( $debug ) {
    echo "<pre>";
    var_dump($manager);
    echo "</pre>";
  }
?>
