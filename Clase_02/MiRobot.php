<?php
class Robot {
	const ROBOT_APAGADO = "El robot esta apagado";
	const CAMINE_HACIA = "Caminé a la";
	
	private $_nombreR;
	private $_girar;
	
	public function __construct($nombre){
		$this->_nombreR=$nombre;
		$this->_prendido=true;
		$this->_girar = '';
	}
	
	public function obtenerNombre(){
		return $this->_nombreR;
	}
	
	public function caminar($irHacia){
		if(!$this->_prendido){
			return self::ROBOT_APAGADO;
		}else{ 
			$this->_girar=$irHacia;
			return self::CAMINE_HACIA . ' ' . $this->_girar;
		}
	}
	
	public function activo($activo){
		$this->_prendido=$activo;
	}
	
	public function estaPrendido() {
		return $this->_prendido;	
	}
}

$miRobot= new Robot("Fedebot");
echo 'Mi nombre es ' . $miRobot->obtenerNombre() . "<br />";
echo $miRobot->caminar("derecha") . "<br />";
$miRobot->activo(false);
echo $miRobot->caminar("izquierda") . "<br />";