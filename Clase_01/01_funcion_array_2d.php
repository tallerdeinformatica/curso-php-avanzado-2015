<?php

function createArray2D( $filas, $columnas ) {
	$arr = array();

	for( $i = 0; $i < $filas; $i++ ) {
		$arr[$i] = array();
		
		for( $j = 0; $j < $columnas; $j++ ) {
			$arr[$i][$j] = 0;	
		}
	}
	
	return $arr;
}

$mapa = createArray2D(3,3);
$mapa[0][2] = 1;

echo '<pre>';
var_dump($mapa);
echo '</pre>';