<?php
/**
 * Definimos la función y le pasamos como parametro el array a ordenar
 */
function ordenarNombres(array $nombres) {
	$nombresCant = count($nombres);
	$sorted = false;
	
	while( false === $sorted ) {
		$sorted = true;
		for ($i = 0; $i < $nombresCant-1; ++$i) {
			$current = $nombres[$i];
			$next = $nombres[$i+1];
			if (strlen($next) > strlen($current)) {
				$nombres[$i] = $next;
				$nombres[$i+1] = $current;
				$sorted = false;
			}
		}
	}
	
	return $nombres;
}

/*
 * Utilización de la función
 */
$testNombres = array("Alfredo", "Joaquin", "Maxi", "Andres", "Lolo", "Federico", "Lucas", "Rodolfo", "Antonio");
$resultado = ordenarNombres($testNombres);

echo '<pre>';
var_dump($resultado);
echo '</pre>';