<?php
  include_once('app/models/Usuario.php');

  $usuario = new Usuario();
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Administración</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="container">
    <h1 style="margin-top:5%;">Administración // Busqueda</h1>
    <div class="row">
      <p style="background-color:#F3F3F3; padding:5px; text-align:center;">
<?php
    if(isset($_GET["msg"]))
    {
        switch($_GET["msg"]) {
          case "user_deleted":
            echo "El usuario fue borrado exitosamente!";
            break;
        }
    }
?>
      </p>
    </div>
    <div class="row" style="text-align:center;">
      <form method="POST" action="busqueda.php">
        <div class="row">
            <label for="busquedaInput">Buscar:</label>
            <input type="text" name="nombre" placeholder="Juan" id="busquedaInput">
            <select name="tipoBusquedaSelect">
              <option value="1">Nombre exacto</option>
              <option value="2">Empiece con...</option>
              <option value="3">Termine con...</option>
          </select>
          <input type="submit" class="button button-primary" value="Buscar"/>
        </div>
      </form>
    </div>
    <div class="row" style="text-align:center;">
      <a class="button button-primary" href="index.php">Volver</a>
    </div>
    <div class="row">
      <table class="u-full-width">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Dirección</th>
            <th>Teléfono</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
<?php
  /*
   * Listado de usuarios
   */
  $filtroBusqueda = '';
  $tipoFiltro = Usuario::EMPIEZA_CON;

  if( isset($_POST['nombre']) ) {
    $filtroBusqueda = $_POST['nombre'];
  }

  if( isset($_POST['tipoBusquedaSelect']))
  {
    $tipo = $_POST['tipoBusquedaSelect'];

    switch( $tipo ) {
      case 1:
        $tipoFiltro = Usuario::NOMBRE_EXACTO;
        break;
      case 2:
        $tipoFiltro = Usuario::EMPIEZA_CON;
        break;
      case 3:
        $tipoFiltro = Usuario::TERMINA_CON;
        break;
    }
  }

  $listaUsuarios = $usuario->filtrarUsuarios($filtroBusqueda, $tipoFiltro);

  if ($listaUsuarios)
  {
    foreach( $listaUsuarios as $usr ) {
?>
        <tr>
          <td><?php echo $usr->nombre ?></td>
          <td><?php echo $usr->direccion ?></td>
          <td><?php echo $usr->telefono ?></td>
          <td>
            <a class="button" href="edicion.php?id=<?php echo $usr->id ?>">Editar</a>
            <a class="button" href="borrado.php?id=<?php echo $usr->id ?>">Eliminar</a>
          </td>
        </tr>
<?php
    }
  }
?>
        </tbody>
      </table>
    </div>
  </div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>