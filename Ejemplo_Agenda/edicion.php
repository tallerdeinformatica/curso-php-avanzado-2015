<?php
  include_once('app/models/Usuario.php');
  include_once('app/models/Robot.php');

  $usuario = new Usuario();

  $userId = filter_var( $_GET['id'], FILTER_VALIDATE_INT );
  $userData = $usuario->obtenerUsuario($userId);

  $robot = new Robot();
  $robotData = $robot->obtenerRobotUsuario($userId);
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Administración</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="container">
    <h1 style="margin-top:5%;">Administración // Editar usuario</h1>
    <form action="app/edicion_usuario.php" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="idUsuario" value="<?php echo $userData->id; ?>">
      <div class="row">
          <p>Imagen</p>
          <img src="upload/<?php echo $robotData->avatar; ?>" width="200" height="200" />
      </div>
      <div class="row">
          <label for="nombreInput">Nombre</label>
          <input class="u-full-width" type="text" name="nombre" value="<?php echo $userData->nombre; ?>" id="nombreInput">
      </div>
      <div class="row">
        <label for="emailInput"><em>*</em> E-mail</label>
        <input class="u-full-width" type="email" name="email" value="<?php echo $userData->email; ?>" id="emailInput" required>
      </div>
      <div class="row">
        <label for="direccionInput">Dirección</label>
        <input class="u-full-width" type="text" name="direccion" value="<?php echo $userData->direccion; ?>" id="direccionInput">
      </div>
      <div class="row">
        <label for="telefonoInput">Teléfono</label>
        <input class="u-full-width" type="text" name="telefono" value="<?php echo $userData->telefono; ?>" id="telefonoInput">
      </div>
      <div class="row">
        <label for="robotNameInput"><em>*</em> Nombre del robot</label>
        <input class="u-full-width" type="text" name="nombreRobot" value="<?php echo $robotData->nombre; ?>" id="robotNameInput" required>
      </div>
       <div style="text-align:center;">
        <input id="imagenRobot" name="imagenRobot" type="file" />
      </div>
      <div style="text-align:center;">
        <input class="button-primary" type="submit" value="Editar">
        <a class="button" style="background-color:#F3F3F3;" href="index.php">Volver</a>
      </div>


    </form>
  </div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>