<?php
include_once('models/Usuario.php');
include_once('models/Robot.php');

if( isset($_POST) && isset($_FILES) )
{
  $data = new stdClass();
  $data->nombre    = filter_var($_POST['nombre'], FILTER_SANITIZE_SPECIAL_CHARS);
  $data->email     = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
  $data->direccion = filter_var($_POST['direccion'], FILTER_SANITIZE_SPECIAL_CHARS);
  $data->telefono  = filter_var($_POST['telefono'], FILTER_SANITIZE_SPECIAL_CHARS);
  $data->nombreRobot  = filter_var($_POST['nombreRobot'], FILTER_SANITIZE_SPECIAL_CHARS);

  $imagenRobot = $_FILES['imagenRobot'];

  $tempFile = $imagenRobot['tmp_name'];
  $nombreArchivo = filter_var($imagenRobot['name'], FILTER_SANITIZE_SPECIAL_CHARS);
  $destinoFinal = dirname(__FILE__) . '/../upload/';

  $partesNombres = explode('.', strtolower($nombreArchivo));
  $nuevoNombreImagen = str_replace(' ', '_', $partesNombres[0]);
  $extension = end($partesNombres);
  $nuevoNombreImagen.= '_' . md5(uniqid()) . '.' . $extension;

  $archivoFinal = $destinoFinal . $nuevoNombreImagen;

  $tiposArchivo = array('jpg', 'jpeg', 'gif', 'png');

  if( $data->nombre && $data->email && $data->nombreRobot) {
    $robot = new Robot();
    $usuario = new Usuario();
    $lastUserId = 0;

    if( $usuario->agregarUsuario( $data ) ) 
    {
      $lastUserId = $usuario->obtenerIdUltimoRegistro();

      $robotData = new stdClass();
      $robotData->nombre = $data->nombreRobot;
      $robotData->avatar = $nuevoNombreImagen;

      if( $robot->agregarRobot( $lastUserId, $robotData ) ) {

        if( in_array($extension, $tiposArchivo) ) {
          //Hago la subida
          if( copy($tempFile, $archivoFinal) ) {
            header("location:../index.php?msg=exito");
          }else{
            header("location:../index.php?msg=error_copiando_archivo");
          }
        }else{
          // La extension no está permitida
          header("location:../index.php?msg=error_extension_archivo");
        }

      }else{
        header("location:../index.php?msg=error_creando_robot");
      }

    }else{
      header("location:../index.php?msg=error_insertar");
    }
  }else{
     header("location:../index.php?msg=error_datos_requeridos");
     // Los campos con * son requeridos
  }

}else{
  header("location:../index.php?msg=no_data");
}