<?php
include_once("Model.php");
/**
 * Usuario
 */
class Usuario extends Model
{
    const NOMBRE_EXACTO = 'exact';
    const EMPIEZA_CON = 'init';
    const TERMINA_CON = 'final';

    // Variable privada que almacena el objeto PDO
    private $_db;

    private $_lastInsertedId = 0;

    /**
     * [__construct description]
     */
    public function __construct() {
        // Creamos una nueva conexión
        $this->_db = Model::getInstance();
    }

    public function listarUsuarios() {
      $sql = null;

      try {
            $sql = $this->_db->query("SELECT * FROM usuarios");
            $sql->setFetchMode(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $sql;
    }

    public function filtrarUsuarios( $busqueda, $tipo_filtro = self::NOMBRE_EXACTO ) {
      $result = null;

      try {
            $filtrado = "='".$busqueda."'";

            switch( $tipo_filtro ) {
              case self::NOMBRE_EXACTO:
                $filtrado = $busqueda;
                break;
              case self::EMPIEZA_CON:
                $filtrado = $busqueda ."%";
                break;
              case self::TERMINA_CON:
                $filtrado = "%" . $busqueda;
                break;
            }

            $sql = $this->_db->prepare("SELECT * FROM usuarios WHERE nombre LIKE :Filtrado");
            $sql->execute(array(
              "Filtrado" => $filtrado
            ));
            $sql->setFetchMode(PDO::FETCH_OBJ);
            $result = $sql->fetchAll();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    public function obtenerUsuario( $id_usuario ) {
      $result = null;

      try {
          $sql = $this->_db->prepare("SELECT * FROM usuarios WHERE id=:IdUsuario");
          $sql->execute(array( "IdUsuario" => $id_usuario ));
          $sql->setFetchMode(PDO::FETCH_OBJ);
          $result = $sql->fetch();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    public function agregarUsuario( stdClass $data ) {
        $sql = null;
        $this->_lastInsertedId = 0;

        try {
            $sql= $this->_db->prepare ("INSERT INTO usuarios(nombre, direccion, email, telefono) VALUES(:Nombre, :Direccion, :Correo, :Telefono)");
            $sql->execute(array(
              "Nombre"    => $data->nombre,
              "Direccion" => $data->direccion,
              "Correo"    => $data->email,
              "Telefono"  => $data->telefono
            ));

            $this->_lastInsertedId = $this->_db->lastInsertId();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return ($sql->rowCount() > 0) ? true : false;
    }

    public function editarUsuario( $userid, stdClass $data ) {
        $sql = null;

        try {
            $sql= $this->_db->prepare ("UPDATE usuarios SET nombre=:Nombre, direccion=:Direccion, email=:Correo, telefono=:Telefono WHERE id=:IdUsuario");
            $sql->execute(array(
              "Nombre"    => $data->nombre,
              "Direccion" => $data->direccion,
              "Telefono"  => $data->telefono,
              "Correo"    => $data->email,
              "IdUsuario" => $userid
            ));
        } catch(PDOException $e) {
            echo $e->getMessage();
        }

        return ($sql->rowCount() > 0) ? true : false;
    }

    public function eliminarUsuario( $id_usuario ) {
        try {
            $sql = $this->_db->prepare("DELETE FROM usuarios WHERE id=:IdUsuario");
            $sql->execute(array( "IdUsuario" => $id_usuario ));
            return ($sql->rowCount() > 0) ? true : false;
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }
    }

    /**
     * Retorna el ID del último registro insertado
     * @return int
     */
    public function obtenerIdUltimoRegistro() {
      return (int) $this->_lastInsertedId;
    }
}