<?php
include_once('Model.php');

/**
 * Usuario
 */
class Robot extends Model
{
    // Variable privada que almacena el objeto PDO
    private $_db;

    /**
     * [__construct description]
     */
    public function __construct() {
        // Creamos una nueva conexión
        $this->_db = self::getInstance();
    }

    public function listarRobots() {
      $sql = null;

      try {
            $sql = $this->_db->query("SELECT * FROM robots");
            $sql->setFetchMode(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $sql;
    }

    public function filtrarRobots( $busqueda, $tipo_filtro = self::NOMBRE_EXACTO ) {
      $result = null;

      try {
            $filtrado = "='".$busqueda."'";

            switch( $tipo_filtro ) {
              case self::NOMBRE_EXACTO:
                $filtrado = $busqueda;
                break;
              case self::EMPIEZA_CON:
                $filtrado = $busqueda ."%";
                break;
              case self::TERMINA_CON:
                $filtrado = "%" . $busqueda;
                break;
            }

            $sql = $this->_db->prepare("SELECT * FROM robots WHERE nombre LIKE :Filtrado");
            $sql->execute(array(
              "Filtrado" => $filtrado
            ));
            $sql->setFetchMode(PDO::FETCH_OBJ);
            $result = $sql->fetchAll();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    public function obtenerRobot( $id_robot ) {
      $result = null;

      try {
          $sql = $this->_db->prepare("SELECT * FROM robots WHERE id=:IdRobot");
          $sql->execute(array( "IdRobot" => $id_robot ));
          $sql->setFetchMode(PDO::FETCH_OBJ);
          $result = $sql->fetch();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    public function obtenerRobotUsuario( $id_usuario ) {
      $result = null;

      try {
          $sql = $this->_db->prepare("SELECT * FROM robots WHERE id_usuario=:IdUsuario");
          $sql->execute(array( "IdUsuario" => $id_usuario ));
          $sql->setFetchMode(PDO::FETCH_OBJ);
          $result = $sql->fetch();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    /**
     * Agregamos un robot
     * @param  stdClass $data
     * @return Boolean
     */
    public function agregarRobot( $userid, stdClass $data ) {
        $sql = null;

        try {
            $sql= $this->_db->prepare ("INSERT INTO robots (id_usuario, nombre, avatar) VALUES(:Id, :Nombre, :Avatar)");
            $sql->execute(array(
              "Id"    => (int) $userid,
              "Nombre" => $data->nombre,
              "Avatar" => $data->avatar
            ));
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return ($sql->rowCount() > 0) ? true : false;
    }
   
    public function editarRobot( $robotid, stdClass $data ) {
        $sql = null;

        try {
            $sql= $this->_db->prepare ("UPDATE robots SET nombre=:Nombre, avatar=:Avatar, email=:Email WHERE id=:IdRobot");
            $sql->execute(array(
              "Nombre"    => $data->nombre,
              "Avatar"    => $data->avatar,
              "Email"     => $data->email,
              "IdRobot"   => $robotid
            ));
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return ($sql->rowCount() > 0) ? true : false;
    }
}