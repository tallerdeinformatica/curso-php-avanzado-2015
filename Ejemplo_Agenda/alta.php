<?php
  include_once('app/models/Usuario.php');

  $usuario = new Usuario();
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Administración</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <div class="container">
    <h1 style="margin-top:5%;">Administración // Agregar usuario</h1>
    <form action="app/agregar_usuario.php" method="POST" enctype="multipart/form-data">
      <div class="row">
          <label for="nombreInput"><em>*</em> Nombre</label>
          <input class="u-full-width" type="text" name="nombre" placeholder="Juan" id="nombreInput" required>
      </div>
      <div class="row">
        <label for="emailInput"><em>*</em> E-mail</label>
        <input class="u-full-width" type="email" name="email" placeholder="nombre@dominio.com" id="emailInput" required>
      </div>
      <div class="row">
        <label for="direccionInput">Dirección</label>
        <input class="u-full-width" type="text" name="direccion" placeholder="Acevedo Díaz" id="direccionInput">
      </div>
      <div class="row">
        <label for="telefonoInput"><em>*</em> Teléfono</label>
        <input class="u-full-width" type="number" name="telefono" placeholder="094333222" id="telefonoInput" required>
      </div>
      <div class="row">
        <label for="robotNameInput"><em>*</em> Nombre del robot</label>
        <input class="u-full-width" type="text" name="nombreRobot" placeholder="RDD2" id="robotNameInput" required>
      </div>

      <div style="text-align:center;">
        <input id="imagenRobot" name="imagenRobot" type="file" />
      </div>
      
      <div style="text-align:center;">
        <a class="button" style="background-color:#F3F3F3;" href="index.php">Volver</a>
        <input class="button-primary" type="submit" value="Agregar">
      </div>

    </form>
  </div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>