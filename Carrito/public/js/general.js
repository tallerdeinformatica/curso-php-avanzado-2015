$(document).ready(function()
{
    var cart = new Cart();

    var productItems  = $('.addItem'),
        btnEmpty      = $('.emptyCart'),
        btnQuantities = $('.quantButton'),
        btnRemove     = $('.eliminar'),
        emSubtotal    = $('#subtotal'),
        btnBuy        = $('#btnBuy');

    /**
     * Agregar al carro
     */
    productItems.on('click', function(e)
    {
        var parent = $(this).parent();
        cart.addItem( parent );
    });

    /**
     * Botón Vaciar carro
     */
    btnEmpty.on('click', function(e)
    {
        e.preventDefault();
        var deleteAll = confirm('¿Deseas borrar el contenido del carro?');

        if( deleteAll ) {
            $.ajax({
                url : 'app/emptyCart.php',
                success : function( result )
                {
                    if( result )
                        $('#liCart').html( '¡Tu carro está vacío!' );
                }
            });
        }
    });

    /**
     * Botón Aumentar/Remover cantidad
     */
    btnQuantities.on('click', function(e)
    {
        var adding          = ($(this).hasClass('add')) ? 1 : 0,
            quantityBox     = null,
            currentQuantity = 0,
            parent          = $(this).parent()
            productId       = 0,
            currentPrice    = 0,
            subtotal        = 0;

        quantityBox     = parent.find('.txtQuantity');
        currentQuantity = parseInt( quantityBox.val() );
        currentPrice    = parseInt( parent.parent().find('.prices').html() );
        subtotal        = parseInt( emSubtotal.html() );

        productId = parseInt(parent.find('.productId').val());

        $.ajax({
            url  : 'app/itemQuantity.php',
            type : 'POST',
            data : 'id='+productId + '&adding=' + adding,
        });

        if( adding ) {
            currentQuantity++;
            subtotal += currentPrice;
        }else{
            currentQuantity--;
            subtotal -= currentPrice;
        }

        quantityBox.val( currentQuantity );
        emSubtotal.html( subtotal );
    });

    /**
     * Botón Eliminar elemento del carro
     */
    btnRemove.on('click', function(e)
    {
        var parent      = $(this).parent(),
            superParent = $(this).parents('tr');

        var quantityBox     = null,
            currentQuantity = 0,
            productId       = 0,
            currentPrice    = 0,
            subtotal        = 0;

        quantityBox     = superParent.find('.txtQuantity');
        productId       = parseInt( superParent.find('.productId').val() );
        currentQuantity = parseInt( quantityBox.val() );
        currentPrice    = parseInt( superParent.find('.prices').html() );
        subtotal        = parseInt( emSubtotal.html() );

        subtotal -= currentQuantity * currentPrice;

        $.ajax({
            url  : 'app/removeItem.php',
            type : 'POST',
            data : 'id=' + productId,
            dataType : 'json',
            success : function( result )
            {
                if (result.status) superParent.fadeOut();
            },
            complete : function() {
                emSubtotal.html( subtotal );
            }
        });

    });

    /**
     * Botón "COMPRAR"
     */
    btnBuy.on('click', function(e)
    {
        e.preventDefault();

        $.ajax({
            url  : 'app/cartCheckout.php',
            type : 'POST',
            dataType : 'json',
            success : function( result )
            {
                console.log( result );
            }
        });
    });
});

/**
 * Cart Class
 */
Cart = function() {};

Cart.prototype = Object.create({});
Cart.prototype.constructor = Cart;

Cart.prototype.addItem = function( item ) {

    var item_id, item_price, item_name;

    item_id    = parseInt(item.find('.id').val());
    item_price = parseInt(item.find('.price').val());
    item_name  = item.find('span').html();

    if( item_id && item_price ) {
        $.ajax({
            url : 'app/addItem.php',
            type: 'POST',
            dataType : 'json',
            data : 'id=' + item_id,

            beforeSend : function() {
                item.find('.loader').fadeIn();
            },

            success : function( result )
            {
                if( result.status )
                {
                    var items    = result.incart,
                        itemsLen = items.length,
                        itemsQuantity = 0;

                    for( var i = 0; i < itemsLen; i++ )
                        itemsQuantity += items[i].quantity;

                    if( itemsQuantity > 0 ) {
                        var str = 'Tienes ' + itemsQuantity + ' articulo/s en el carro.';
                        str += '<a href="checkout.php" class="cart-buttons">Checkout</a>';
                        str += '<a href="#" class="cart-buttons danger emptyCart">Vaciar</a>';
                        $('#liCart').html( str );
                    }
                }
            },

            complete : function() {
                item.find('.loader').fadeOut();
            }
        });
    }
};