<?php
include_once('../inc/includes.php');

$response = new stdClass();
$response->message  = "No puede acceder a este archivo";
$response->status   = false;
$response->subtotal = 0;

if( isset($_SESSION["userCart"]) ) 
{
    $product = new Product();
    $subtotal = 0;

    foreach( $_SESSION["userCart"] as $item ) {
        $price = $product->getPriceByProduct( $item->id )->price;
        $subtotal += $price * $item->quantity;
    }

    $response->message  = 'OK';
    $response->status   = true;
    $response->subtotal  = $subtotal;    
}else{
    $response->message = "Se produjo un error";
    $response->status  = false;
}

header("Content-Type:application/json");
echo json_encode($response);