<?php
include_once('../inc/includes.php');

$response = new stdClass();
$response->message = "No puede acceder a este archivo";
$response->status = false;

if (isset($_POST))
{
    $itemId = (int) $_POST['id'];

    if ($itemId)
    {
        $counter = 0;
        foreach( $_SESSION["userCart"] as $item ) {
            if( $item->id === $itemId ) {
                unset($_SESSION["userCart"][$counter]);
                break;
            }

            $counter++;
        }

        $response->message  = 'OK';
        $response->status   = true;
    } else {
        $response->message = 'Se produjo un error';
        $response->status = false;
    }
}

header("Content-Type:application/json");
echo json_encode($response);