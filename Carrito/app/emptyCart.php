<?php
session_start();

$response = new stdClass();

unset($_SESSION['userCart']);

$response->message = 'OK';
$response->status  = true;

header("Content-Type:application/json");
echo json_encode($response);