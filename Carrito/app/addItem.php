<?php
include_once('../inc/includes.php');

$response = new stdClass();
$response->message = "No puede acceder a este archivo";
$response->status = false;

if (isset($_POST))
{
    $product = new Product();

    $itemId = (int) $_POST['id'];

    $productDetail = $product->getProductById( $itemId );

    if( $productDetail )
    {
        $itemPrice  = (int) $productDetail->price;
        $itemName   = $productDetail->name;

        if ($itemId && $itemPrice && $itemName)
        {
            $incartItem = new stdClass();

            $incartItem->id       = $itemId;
            $incartItem->name     = $itemName;
            $incartItem->price    = $itemPrice;
            $incartItem->quantity = 1;
            /* ------------------------------------------*/

            $arrayIds     = array();
            $idsCount     = 0;
            $position     = -1;
            $added        = false;

            foreach( $_SESSION["userCart"] as $item ) {
                array_push( $arrayIds, $item->id );
            }

            $idsCount = count($arrayIds);

            for( $i = 0; $i < $idsCount; $i++ ) {
                if( $arrayIds[$i] === $incartItem->id ) {
                    $position = $i;
                    break;
                }
            }

            if( $position != -1 ) $added = true;

            /* ------------------------------------------*/
            if( !$added )
            {
                if( array_push($_SESSION["userCart"], $incartItem ) ) {
                    $response->message = 'ok';
                    $response->status = true;
                    $response->incart = $_SESSION["userCart"];
                }else{
                    $response->message = 'No se pudo agregar al array';
                    $response->status = false;
                }
            }else{
                if( $_SESSION["userCart"][$position] )
                {
                    $_SESSION["userCart"][$position]->quantity++;

                    $response->message = 'ok';
                    $response->status = true;
                    $response->incart = $_SESSION["userCart"];
                }
            }

        } else {
            $response->message = 'Se produjo un error';
            $response->status = false;
        }
    }else{
        $response->message = 'El producto no existe';
        $response->status = false;
    }

}

header("Content-Type:application/json");
echo json_encode($response);