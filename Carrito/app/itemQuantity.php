<?php
include_once('../inc/includes.php');

$response = new stdClass();
$response->message = "No puede acceder a este archivo";
$response->status = false;

if (isset($_POST))
{
    $itemId = (int) $_POST['id'];
    $add    = (int) $_POST['adding'];

    if ($itemId)
    {
        $counter = 0;
        foreach( $_SESSION["userCart"] as $item ) {
            if( $item->id === $itemId ) {

                $value = $_SESSION["userCart"][$counter]->quantity;
                if ($add) {
                    $value++;
                }else{
                    $value--;
                }

                if( $value < 1 ) $value = 1;
                if( $value > 99 ) $value = 99;

                $_SESSION["userCart"][$counter]->quantity = $value;
                break;
            }

            $counter++;
        }

        $response->message  = 'OK';
        $response->status   = true;
        $response->quantity = $_SESSION["userCart"][$counter]->quantity;
    } else {
        $response->message = 'Se produjo un error';
        $response->status = false;
    }
}

header("Content-Type:application/json");
echo json_encode($response);