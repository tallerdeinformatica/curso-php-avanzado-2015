<?php
include_once("Model.php");
/**
 * Product
 */
class Product extends Model
{
    // Variable privada que almacena el objeto PDO
    private $db;
    private $db_name = 'products';

    public function __construct()
    {
        $this->db = Model::getInstance(); // Creamos una nueva conexión
    }

    public function getProductById( $id ) {
      $result = null;

      try {
          $sql = $this->db->prepare("SELECT * FROM $this->db_name WHERE id=:ProductId");
          $sql->execute(array( "ProductId" => $id ));
          $sql->setFetchMode(PDO::FETCH_OBJ);
          $result = $sql->fetch();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    public function getPriceByProduct( $id ) {
      $result = null;

      try {
          $sql = $this->db->prepare("SELECT price FROM $this->db_name WHERE id=:ProductId");
          $sql->execute(array( "ProductId" => $id ));
          $sql->setFetchMode(PDO::FETCH_OBJ);
          $result = $sql->fetch();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }

    public function getList() {
      $sql = null;

      try {
            $sql = $this->db->query("SELECT * FROM $this->db_name WHERE available=1 AND stock !=0");
            $sql->setFetchMode(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $sql->fetchAll();
    }

}