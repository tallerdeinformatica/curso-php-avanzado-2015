<?php
include_once("Model.php");
/**
 * User
 */
class User extends Model
{
    // Variable privada que almacena el objeto PDO
    private $db;

    public function __construct()
    {
        $this->db = Model::getInstance(); // Creamos una nueva conexión
    }

    public function listarUsuarios()
    {
      $sql = null;

      try {
            $sql = $this->db->query("SELECT * FROM users");
            $sql->setFetchMode(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $sql;
    }

    public function chequearUsuario( $nombre, $password )
    {
      $result = null;

      try {
        $sql = $this->db->prepare("SELECT id, name, email FROM users WHERE name = :Nombre AND password = MD5(:Password)");
        $sql->execute(array(
          "Nombre"    => $nombre,
          "Password"  => $password
        ));
        $sql->setFetchMode(PDO::FETCH_OBJ);
        $result = $sql->fetchAll();
      }catch(PDOException $e) {
        $this->_error = $e->getMessage();
      }

      return $result;
    }

    public function obtenerUsuario( $id_usuario )
    {
      $result = null;

      try {
          $sql = $this->db->prepare("SELECT * FROM users WHERE id=:IdUsuario");
          $sql->execute(array( "IdUsuario" => $id_usuario ));
          $sql->setFetchMode(PDO::FETCH_OBJ);
          $result = $sql->fetch();
        } catch(PDOException $e) {
            $this->_error = $e->getMessage();
        }

        return $result;
    }
}