<?php
  include_once('inc/includes.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>PHP Avanzado // Carrito de compras</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="public/css/normalize.css">
  <link rel="stylesheet" href="public/css/skeleton.css">
  <link rel="stylesheet" href="public/css/general.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="public/js/general.js"></script>
</head>
<body>
  <!-- Start Wrapper -->
  <div class="wrapper">
    <header class="mainHeader">
      <h1>Ejemplo de Carrito</h1>
    </header>
    <nav class="mainNav">
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="mycart.php">Mi Carrito</a></li>
<?php
          $totalItems = 0;

          foreach( $_SESSION['userCart'] as $product ) {
              $totalItems += $product->quantity;
          }

          if( $totalItems <= 0 ) {
?>
          <li id="liCart">¡Tu carro está vacío!</li>
<?php
          }else{
?>
          <li id="liCart">Tienes <?php echo $totalItems; ?> articulo/s en el carro.<a href="checkout.php" class="cart-buttons">Checkout</a><a href="#" class="cart-buttons danger emptyCart">Vaciar</a></li>
<?php
          }
?>
        </ul>
    </nav>
    <div class="content">

      <ul class="productsList">
<?php
      $productos = new Product();
      $productsList = $productos->getList();

      # Listando los productos con un foreach
      foreach( $productsList as $item ) :
?>
        <li class="productItem">

          <div class="loader"><img src="public/images/ajax-loader.gif"></div>

          <input type="hidden" class="id" value="<?php echo $item->id; ?>" />
          <input type="hidden" class="price" value="<?php echo $item->price; ?>" />
          <span><?php echo $item->name; ?></span>
          <img src="public/images/100x100.png">
          <p><?php echo $item->description; ?></p>
          <em><?php echo $item->price; ?> USD</em>
          <button class="addItem">Agregar al carrito</button>
        </li>
<?php endforeach; ?>
      </ul>
    </div>
  </div>
  <!-- End Wrapper -->
</body>
</html>
