<?php
session_start();

#$_SESSION["userCart"] = array();
#die();

if( !isset($_SESSION["userCart"]) ) {
    $_SESSION["userCart"] = array();
}

define( 'ROOT_PATH' , dirname(__FILE__).'/../' );

include_once( ROOT_PATH . "app/models/Model.php");

include_once( ROOT_PATH . "app/models/Product.php");
include_once( ROOT_PATH . "app/models/User.php");