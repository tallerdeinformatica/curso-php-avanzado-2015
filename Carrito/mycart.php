<?php
  include_once('inc/includes.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>PHP Avanzado // Carrito de compras</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="public/css/normalize.css">
  <link rel="stylesheet" href="public/css/skeleton.css">
  <link rel="stylesheet" href="public/css/general.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="public/js/general.js"></script>
</head>
<body>
  <!-- Start Wrapper -->
  <div class="wrapper">
    <header class="mainHeader">
      <h1>Ejemplo de Carrito</h1>
    </header>
    <nav class="mainNav">
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="mycart.php">Mi Carrito</a></li>
<?php
          $totalItems = 0;

          foreach( $_SESSION['userCart'] as $product )
            $totalItems += $product->quantity;
          }

          if( $totalItems <= 0 ) {
?>
          <li id="liCart">¡Tu carro está vacío!</li>
<?php
          }else{
?>
          <li id="liCart">Tienes <?php echo $totalItems; ?> articulo/s en el carro.<a href="checkout.php" class="cart-buttons">Checkout</a><a href="#" class="cart-buttons danger emptyCart">Vaciar</a></li>
<?php
          }
?>
        </ul>
    </nav>
    <div class="content">
      <table class="u-full-width">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
<?php
      # Listando los productos con un foreach
      $totalPrice = 0;

      foreach( $_SESSION['userCart'] as $item ) :
        $totalPrice += $item->price * $item->quantity;
?>
        <tr>
          <td><?php echo $item->name; ?></td>
          <td class="prices"><?php echo $item->price; ?></td>
          <td>
            <button class="quantButton remove">-</button>
            <input class="txtQuantity" type="number" value="<?php echo $item->quantity; ?>" disabled style="width:70px"/>
            <input class="productId" type="hidden" value="<?php echo $item->id; ?>"/>
            <button class="quantButton add">+</button>
          </td>
          <td><button class="eliminar" id="item_<?php echo $item->id; ?>">Eliminar</button></td>
        </tr>
<?php endforeach; ?>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td>Total: <em id="subtotal"><?php echo $totalPrice; ?></em></td>
        </tr>
        </tbody>
      </table>
      <div style="text-align:center">
        <a href="checkout.php" class="button button-primary">Checkout</a>
      </div>
    </div>
  </div>
  <!-- End Wrapper -->
</body>
</html>
